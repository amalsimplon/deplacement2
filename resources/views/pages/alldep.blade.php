@extends('layouts.app') 

@section('content')
<br/>
<h1>tous les deplacements</h1>

@if(count($deplacements)> 0)

<div class="well">
	<table class="table table-hover">
		<thead>
			<tr>
				<th scope="col">NOM </th>
				<th scope="col">PRÉNOM</th>
				<th scope="col">DATE DÉPART</th>
				<th scope="col">HEURE DÉPART</th>
				<th scope="col">DATE RETOUR</th>
				<th scope="col">HEURE RETOUR</th>
				<th scope="col">VILLE</th>
				<th scope="col">MOYEN</th>
				<th scope="col"></th>
				<th scope="col"></th>
			</tr>
		</thead>
		<tbody>

			@foreach($deplacements as $deplacement)
			<tr>
				<td>{{ $deplacement->nom }}</td>
				<td>{{ $deplacement->prenom }}</td>
				<td>{{ $deplacement->datedepart }}</td>
				<td>{{ $deplacement->heuredepart}}</td>
				<td>{{ $deplacement->dateretour }}</td>
				<td>{{ $deplacement->heureretour}}</td>
				<td>{{ $deplacement->ville->nom}}</td>
				
				<td>{{ $deplacement->idMoyen }}</td>
				
				
				<td><a href="/pages/{{$deplacement->id}}/edit" class="btn btn-default">modifier</a>
				</td>
				<td>{!! Form::open(['action' => ['depcontroller@destroy', $deplacement->id], 'method'=>'POST']) !!} 
					{{Form::hidden('_method', 'DELETE')}} 
					{{Form::submit('supprimer', ['class' =>'btn btn-default'])}} 
					{!! Form::close()!!}
				</td>

			</tr>
		</tbody>
				<h3>{{$deplacement->title}}</h3>
				
					</div>
					@endforeach @else
					<p>pas de deplacements jusqu'à maintenant</p>
					@endif
		</table>			
					
@endsection
					