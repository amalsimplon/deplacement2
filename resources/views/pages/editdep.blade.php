@extends('layouts.app')


@section('content')
<br/>
    <h1>Ajouter un déplacement</h1>
    <div>
    <div class= 'col-md-6'>
    
    	{{ Form::open(['action' => 'depcontroller@store']) }}
			<div class="form-groupe">
				{{Form::label('nom', 'Nom')}}
				{{Form::text('nom', '', ['class' => 'form-control', 'placeholder' => 'Nom'])}}
			</div>
			
			<div class="form-groupe">
				{{Form::label('prenom', 'Prénom')}}
				{{Form::text('prenom', '', ['class' => 'form-control', 'placeholder' => 'Prénom'])}}
			</div >
			<div class= 'form-group row'>
			<div class ='col-sm-5'>
			
				{{Form::label('datedepart', 'Date de départ')}}
				{{Form::date('datedepart', '', ['class' => 'form-control'])}}
			
			</div>
			
			<div class ='col-sm-5'>
			
				{{Form::label('heuredepart', 'Heure de départ')}}
				{{Form::time('heuredepart', '', ['class' => 'form-control'])}}
		
			</div>
			</div>
			
			<div class= 'form-group row'>
			<div class ='col-sm-5'>
		
				{{Form::label('dateretour', 'Date de retour')}}
				{{Form::date('dateretour', '', ['class' => 'form-control'])}}
			</div>
			
			<div class ='col-sm-5'>
			
				{{Form::label('heureretour', 'Heure de retour')}}
				{{Form::time('heureretour', '', ['class' => 'form-control'])}}
		
			</div>
			</div>
			
			
			<div class= 'form-group row'>
			<div class ='col-sm-5'>
			
				{{Form::label('idVille', 'Ville')}}
				{{Form::select('idVille', $villesDropDown, ['class' => 'form-control'])}}
			</div>
			
			
			<div class ='col-sm-5'>
				{{Form::label('idMoyen', 'Moyen')}}
				{{Form::select('idMoyen', $moyensDropDown, ['class' => 'form-control'])}}
			</div>
			</div>	
			<br/>
			{{Form::submit('Ajouter', ['class' => 'btn btn-primary',])}}
		{{ Form::close() }}
		
		
		</div>
		</div>
@endsection        
