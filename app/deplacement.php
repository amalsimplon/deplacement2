<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class deplacement extends Model
{
    //Table name
    
    protected $table = 'deplacements';
    
    //Primary key
    public $primaryKey = 'id';
    
    public function ville(){
        return $this->belongsTo(ville::class, 'id');
    }
}
