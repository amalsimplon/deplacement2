<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\deplacement;
use App\moyen;
use App\ville;

 

class depcontroller extends Controller
{
   
    
    public function index (){
        return view ('pages.index');
    }
    
    
    
    /**
     * Display all deplacements
     * 
     * @return \Illuminate\Http\Response
     */
    public function all (){
        //return view ('pages.alldep');
        $deplacements = deplacement::all();
        return view ('pages.alldep') -> with('deplacements',$deplacements);
    }
    
   
    
    public function view (){
        return view ('pages.viewdep');
    }

    /**
     * show the form for creating a new ressource.
     * 
     * @return \Illuminate\Http\Response
     */
    public function create() {
        
        
        // Dynamic Dropdown list 
        $villesDropDown = ville::pluck('nomville', 'id');
        $moyensDropDown = moyen::pluck('nommoyen', 'id');
        return view('pages.editdep', compact('moyensDropDown'), compact('villesDropDown'));
        return view ('pages.editdep');
    }

    
    /**
     * Store a newly created ressource in storage.
     *
     *@param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $this -> validate($request, [
            'nom' => 'required',
            'prenom' => 'required',
        ]);
        
        // Create deplacement 
        $deplacement = new deplacement; 
        $deplacement -> nom = $request -> input('nom');
        $deplacement -> prenom = $request -> input('prenom');
        $deplacement -> datedepart = $request -> input('datedepart');
        $deplacement -> heuredepart = $request -> input('heuredepart');
        $deplacement -> dateretour = $request -> input('dateretour');
        $deplacement -> heureretour = $request -> input('heureretour');
        $deplacement -> idMoyen = $request -> input('idMoyen');
        $deplacement -> idVille = $request -> input('idVille');
        $deplacement -> save();
        
        return redirect('/all')->with ('success', 'Déplacement créé');
        
    }
    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $deplacements = deplacement::find($id);
        return view('pages.show')->with('deplacements', $deplacements);
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $deplacements = deplacement::find($id);
        return view('pages.viewdep')->with('deplacements', $deplacements);
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this -> validate($request, [
            'nom' => 'required'
        ]);
        // update deplacement
        $deplacement = deplacement::find($id);
        $deplacement -> nom = $request -> input('nom');
        $deplacement -> prenom = $request -> input('prenom');
        $deplacement -> datedepart = $request -> input('datedepart');
        $deplacement -> heuredepart = $request -> input('heuredepart');
        $deplacement -> dateretour = $request -> input('dateretour');
        $deplacement -> heureretour = $request -> input('heureretour');
        $deplacement -> idMoyen = $request -> input('idMoyen');
        $deplacement -> idVille = $request -> input('idVille');
        $deplacement -> save();
        return redirect('/all')->with('success', 'Déplacement modifié');
        
        
        // Dynamic Dropdown list 
        $villesDropDown = ville::pluck('nomville', 'id');
        $moyensDropDown = moyen::pluck('nommoyen', 'id');
        return view('pages.view', compact('moyensDropDown'), compact('villesDropDown'));
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $deplacements = deplacement::find($id);
        $deplacements->delete();
        return redirect('/all')->with('success', 'Déplacement supprimé');
    }

    
    
    
}
