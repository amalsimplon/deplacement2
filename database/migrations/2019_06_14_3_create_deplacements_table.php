<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;


class CreateDeplacementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('deplacements', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nom');
            $table->string('prenom');
            $table->date('datedepart');
            $table->time('heuredepart');
            $table->date('dateretour');
            $table->time('heureretour');
            $table->integer('idMoyen')->unsigned();
            /*$table->foreign('idMoyen')
                  ->references('id')
                  ->on('moyens');*/
            $table->integer('idVille')->unsigned();
            /*$table->foreign('idVille')
                  ->references('id')
                  ->on('villes');*/
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('deplacements');
    }
}
